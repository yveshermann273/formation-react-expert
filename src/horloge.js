import React, { Component } from "react";
import DayComponent from "./componnent/Daycomponent";
import HourComponent from "./componnent/HourComponent";
import DateComponent from "./componnent/DateComponent";
import Update from "./componnent/Update";
import Periode from "./componnent/periode";



class Horloge extends Component {
  constructor(props) {
    super(props)
    this.state = {
      time: new Date(),
      isUpdate: false,
      reset: false,
    }
  }

  handleReset = () => {
    this.setState({
      reset: !this.state.reset,
      time: new Date()
    });
  };


  componentDidMount() {

    this.currentTime = setInterval(() => {
      const { time } = this.state
      time.setTime(time.getTime() + 1000);
      this.setState({ time: new Date(time.getTime()) });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.currentTime);
  }



  render() {

    const { time, isUpdate } = this.state
    return (
      <div className="app-container">
        {isUpdate ?

          <Update
            selected={time}
            onChange={(date) => {
              date = (date == null) ? new Date() : date;
              this.setState({ time: date });
            }}
            Back={() => this.setState({ isUpdate: false })}

          /> :
          <div>

            <DayComponent
              reset={this.state.reset}
              initDay={this.state.time.toLocaleDateString([], { weekday: 'long' })}
            />

            <Periode
              time={this.state.time}
            />

            <HourComponent
              reset={this.state.reset}
              initHour={this.state.time.toLocaleTimeString([], { hour: 'numeric', minute: 'numeric' })}

            />
            <DateComponent
              reset={this.state.reset}
              initDate={this.state.time.toLocaleDateString([], { year: 'numeric', month: 'long', day: "numeric" })}
            />
            <button onClick={() => this.setState({ isUpdate: true })} >Modifier</button>
            <button className="btn" onClick={this.handleReset}> Réinitialiser</button>
          </div>

        }

      </div>
    );



  }

}

export default Horloge;