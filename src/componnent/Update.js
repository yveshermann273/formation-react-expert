import React from "react";

import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";


const Update = ({ selected, onChange, Back }) => {
  return (
    <div className="update-block">
      
        <DatePicker
          selected={selected}
          onChange={onChange}
          name="startDate"
          isClearable
          showYearDropdown
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={1}
          timeCaption="time"
          dateFormat="d MMMM yyyy , HH:mm"
          placeholderText="Choisir ici ..."
        />
        <button onClick = {Back}>ok</button>
     
    </div>
  );
};

export default Update;
