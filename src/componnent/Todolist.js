import React,{ useState,useEffect} from 'react';
import Body from "./body"
import Form from "./Form"
import Search from './searchBar';

const TodoList = ()=>{
    const initialState = JSON.parse(localStorage.getItem("todos")) || [];
    const [input, setInput] = useState("");
    const [todos, setTodos]= useState(initialState);
    const [edit, setEdit] = useState(null);
    const [search,setSearch]= useState("");

    useEffect(() => {
        localStorage.setItem("todos",JSON.stringify(todos))
    }, [todos])

    const searchTodoItems = (todos, Search) => {
        const clean = Search.trim();
        if (!clean) {
          return todos;
        }
        return todos.filter((todo) => todo.value.includes(clean));
        
      };

     const  updateInputSearch = (e,input) => {
        setSearch(input);
      };
    
      
    return(
        <div className="container">
            <div className="app-wrapper">
                
                <div>
                    <Search inputSearch={search} onChangeInput={updateInputSearch} />           
                </div>

                <div>
                    <Form 
                        input={input} 
                        setInput={setInput}
                        todos={todos}
                        setTodos={setTodos}
                        edit={edit} 
                        setEdit={setEdit}
                    />
                </div>

                <div>
                    <Body 
                        todos={searchTodoItems(todos,search)} 
                        setTodos={setTodos}
                        setEdit={setEdit}
                    />
                </div>
            </div>
        </div>
    )
}


export default TodoList;