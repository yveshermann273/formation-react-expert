import React,{useState, useEffect} from "react";


export default function DateComponent ({initDate, reset}){

    const [date, setDate] = useState(null);
  


    useEffect(() => {
        setDate(initDate);
      }, [reset, initDate]);


  return <div>{date?.toUpperCase()}</div>;

    

}