import React,{useEffect} from 'react';
import {v4 as uuidv4} from 'uuid';

const Form = ({input,setInput, todos,setTodos,edit,setEdit}) =>{
    
   
    const onChange = (event) =>{
        setInput(event.target.value);
    };

    const updateTodo = (value,id,completed) =>{
        const tache = todos.map((todo) =>
            todo.id === id ? {value,id,completed} : todo
        )
        setTodos(tache)
        setEdit("")
    }
    useEffect(()=>{
        if(edit){
            setInput(edit.value)
        }else{
            setInput("")
        }
    },[setInput,edit]);
    

    const onSubmit = (event) =>{
        event.preventDefault();
        if(!edit){
            setTodos([...todos,{id:uuidv4(), value: input, completed: false}]);
            setInput("");
        }else{
            updateTodo(input, edit.id,edit.completed)
        }

    };

    return (
        <form onSubmit={onSubmit}>
            <input 
                type="text" 
                placeholder="Entrer la tache " 
                className="task-input" 
                value={input} 
                required 
                 onChange={onChange}/>
            <button className="button-add" type="submit">
                {edit?"Modifier" : "Ajouter"}
            </button>
        </form>
    );

}


export default Form;