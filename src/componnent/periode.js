import React from 'react';


const periode = ({time }) => {

let message;

if(time.getHours() <13){
     message = "MATINEE"    
}else if(time.getHours() <18){
     message = "APRES-MIDI" 
}else{
     message = "SOIREE" 
}

return <div>{message.toUpperCase()}</div>

}


export default periode;