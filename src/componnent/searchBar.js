import React from 'react';

const Search = ({inputSearch,onChangeInput}) => {
  
  return (
    <div className="topnav">
   
    <div className="search-container">
        <input
        className="task-input"
          type="text"
          placeholder="recherche"
          value={inputSearch}
          onChange={(e) => onChangeInput(e,e.target.value)}
        />
    
    </div>
  </div>
  );
};



export default Search;