import React,{useState, useEffect} from "react";


export default function DayComponent ({initDay, reset}){

    const [day, setDay] = useState(null);

    useEffect(() => {
        setDay(initDay);
      }, [reset, initDay]);

    
  return <div>{day?.toUpperCase()}</div>;

    

}