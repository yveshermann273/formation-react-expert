import React,{useState, useEffect} from "react";


export default function HourComponent ({initHour, reset}){

    const [hour, setHour] = useState(null);

    useEffect(() => {
        setHour(initHour);
      }, [reset, initHour]);


  return <div>{hour?.toUpperCase()}</div>;


}